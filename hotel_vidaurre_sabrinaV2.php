<?
// mes variables
$nbreChambre = 20;
$login = "saby";
$mdp =1234;
$chambres = []; 
$typeChambre = ['double', 'simple'];


for ($i=0; $i<$nbreChambre; $i++){ // creation de la base
		$chambres [$i] ["numero"] = $i+1;
		$chambres [$i] ["etage"] = random_int (1,3);
		$chambres [$i] ["etat"] = random_int (0,1);
		$chambres [$i] ["type"] = $typeChambre [random_int (0,1)];
		if ($chambres [$i] ["type"] == "double") {
			$chambres [$i] ["prix"] = 120;
		}
		else {
			$chambres [$i] ["prix"] = 90;
		}
}
// var_dump ($chambres [0] ["type"]);	


function nbrechambre ($chambres, $disponibilite, $ouiounon){ // fonction pour le nombre de chambre dispo ou non
	$cpt = 0;
	foreach ($chambres as $dispo) {		
			if ($dispo ["etat"] == $disponibilite) {	
				$cpt = $cpt +1;
			}
	}
	if ($cpt > 0) {
		echo ("le nombre de chambre(s) ".$ouiounon." est de ".$cpt.PHP_EOL);	
	}
	else
	{
		echo ("il n'y a pas de chambre ".$ouiounon.PHP_EOL);
	}		
}

while (true){ // menu qui apparait infiniment
	echo "\n";
	echo " ~~~~ MENU HOTEL louise DWWM PHP ~~~~".PHP_EOL;;
    echo "\n";
    echo("  A - Afficher l'état de l'hôtel \n");
    echo ("  B - Afficher le nombre de chambres réservées \n");
    echo ("  C - Afficher le nombre de chambres libres \n");
    echo ("  D - Afficher le numéro de la première chambre vide \n");
	echo ("  E - Afficher le numéro de la dernière chambre vide \n");
	echo ("  F - Réserver une chambre \n");
	echo ("  G - Libérer une chambre \n");
    echo ("  Q - Quitter \n");
    echo "\n";
    $choix=strtoupper(readline(" Choix  :   "));

	if ((preg_match("#^[a-zA-Z]+$#", $choix))&& 
	($choix== "A" or $choix == "B" or $choix == "C" or $choix == "D" or $choix == "E" or $choix == "F" or $choix== "G" or $choix== "Q")){  //pour filter les lettres (sans caractères, ni chiffre)  
		
			if ($choix=="A"){ //afficher l'etat de l'hotel
				echo "----------------ETAT DE L'HOTEL------------------ ".PHP_EOL;
				
					foreach ($chambres as $index => $chambre){
						
							if ($chambre ["etat"] == 0){
								echo (" - Chambre n° ".$chambre ["numero"].", étage ".$chambre ["etage"].", type ".$chambre ["type"]." : Libre ---> ".$chambre ["prix"]." € ".PHP_EOL);
							
							}
							else {
								echo (" - Chambre n° ".$chambre ["numero"].", étage ".$chambre ["etage"].", type ".$chambre ["type"]." : occupée ---> ".$chambre ["prix"]." € ".PHP_EOL);
							}
						
					}		
				echo "\n";
				echo nbreChambre ($chambres, 1, "réservée(s)");
				echo nbreChambre ($chambres, 0, "vide(s)");
				echo "\n";
				echo"-------------------------------------------------------------".PHP_EOL;
			   
			}
			else if ($choix=="B"){  // afficher le nombre de chambres réservées
				echo nbreChambre ($chambres, 1, "réservée(s)");
				
			}  
			else if ($choix=="C"){ // afficher le nombre de chambres libres
			   echo nbreChambre ($chambres, 0, "vide(s)");
			}		   
			else if ($choix=="D") {  // afficher le numero de la première chambre vide
				$typechoisi = strtoupper(readline ("  Quel type de chambre ? Double (taper D) ou simple (taper S) : "));
				$cpt = 0;
				for ($i=0; $i < $nbreChambre; $i++){
					if (($typechoisi == "D" and ($chambres [$i] ["type"] == "double"))|| ($typechoisi == "S" and ($chambres [$i] ["type"] == "simple"))) {	
						if ($chambres[$i]["etat"]== 0){
							echo ("  La première chambre ".$chambres [$i] ["type"]." libre est la n°".$chambres[$i]["numero"].PHP_EOL);
							break; //chambre trouvé, fin de la boucle
						}
						else {
							$cpt++;
						}
					}
					else {
						$cpt++;
					}
				}
				if ($cpt==$nbreChambre){ // toute les chambres
					echo "  >>> Plus de chambre disponible ou choix incorrect <<< ".PHP_EOL;
				}	
			}
			else if ($choix=="E"){  // afficher le numéro de la dernière chambre vide
				$cpt = 0;
				$typechoisi = strtoupper(readline ("  Quel type de chambre ? Double (taper D) ou simple (taper S) : "));
				for ($i=$nbreChambre-1; $i >= 0; $i--){
					if (($typechoisi == "D" and ($chambres [$i] ["type"] == "double"))|| ($typechoisi == "S" and ($chambres [$i] ["type"] == "simple"))) {			
						if ($chambres[$i]["etat"]==0){	
							echo ("  La dernière chambre ".$chambres [$i] ["type"]." libre est la n°".$chambres[$i]["numero"].PHP_EOL);
							break;
						}
						else {
							$cpt++;
						}
					}
					else {
						$cpt++;
					}
				}
				if ($cpt==$nbreChambre){
					echo "  >>> Plus de chambre disponible ou choix incorrect <<< ".PHP_EOL;
				}	
			}  
			else if ($choix=="F"){ // réserver une chambre
				$loginSaisi = strtolower(readline (" Pour réserver, veuillez saisir votre login : "));
				$mdpsaisi= strtolower(readline (" Veuillez saisir le mot de passe : "));
				$cpt = 0;

					if (($login==$loginSaisi)&&($mdpsaisi==$mdp)) {
						$typechoisi = strtoupper(readline ("  Quel type de chambre ? Double (taper D) ou simple (taper S) : "));
					
						for ($i=0; $i < $nbreChambre; $i++){
					
							if (($chambres[$i]["etat"]==1)||($chambres[$i]["etat"]==0)){
								if ($chambres[$i]["etat"]==1){
									$cpt++;
									continue;
								}
								if (($typechoisi == "D" and ($chambres [$i] ["type"] == "double"))|| ($typechoisi == "S" and ($chambres [$i] ["type"] == "simple"))) {

									$chambres [$i]["etat"]= 1;
									echo ("  La chambre ".$chambres [$i] ["type"]." n° ".$chambres[$i]["numero"]." est réservée... Prix à payer : ".$chambres [$i] ["prix"]. " € ".PHP_EOL);
									break;
								}
								else {
									$cpt++;
									continue;
								}
							} 
						}
						if ($cpt==$nbreChambre){
							echo "  >>> Plus de chambre disponible <<< ".PHP_EOL;
						}					
					}
					else
					{
						echo (" ----> Le mot de passe et/ou login est erroné <----".PHP_EOL);
					}
				
			}
			else if ($choix=="G") {  // liberer une chambre
				$loginSaisi = strtolower(readline ("  Pour libérer une chambre, veuillez saisir votre login : "));
				$mdpsaisi= strtolower(readline ("  Veuillez saisir le mot de passe : "));
				$cpt=0;
						if (($login==$loginSaisi)&&($mdpsaisi==$mdp)) {

							$typechoisi = strtoupper(readline ("  Quel type de chambre ? Double (taper D) ou simple (taper S) : "));

							for ($i=$nbreChambre; $i > 0; $i--){
						
								if (($chambres[$i]["etat"]==1)||($chambres[$i]["etat"]==0)){

									if ($chambres[$i]["etat"]==0){
                                        $cpt++;
										continue;
									}
									if (($typechoisi == "D" and ($chambres [$i] ["type"] == "double"))|| ($typechoisi == "S" and ($chambres [$i] ["type"] == "simple"))) {								
									
										$chambres [$i]["etat"] = 0;
										echo ("  La chambre ".$chambres [$i] ["type"]." n° ".$chambres[$i]["numero"]." est libérée...".PHP_EOL);
										break;
									}
									else {
										$cpt++;
										continue;
									}
								}
							} 
							if ($cpt==$nbreChambre){
								echo "  >>> Plus de chambre occupée <<< ".PHP_EOL;
							}	
						}
						else
						{
							echo (" -----> Le mot de passe et/ou login est erroné <-----".PHP_EOL);
						}
			
			}
			else if ($choix=="Q") { //exit
				echo ("\n ----------Au revoir :'( ------------ \n");
				echo (" \n");
				break;
			}
	}
	else {
			 echo "  :(   Mmmhh, ... Saisie invalide   :(  ".PHP_EOL;
    }
}  
	
?>